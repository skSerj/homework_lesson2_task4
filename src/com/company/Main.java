package com.company;

public class Main {

    public static void main(String[] args) {
        //Написать программу, которая будет проверять, является ли строка словом палиндромом.
        // Программа должна определять палиндром, даже если в слове есть буквы разного регистра (например “Шалаш”, “ПоТоП”).
        // Результат проверки (палиндром или нет) выводить в консоль.
        //4.1** Определять не только слова, но и предложения.
        // Учесть, что предложения могут содержать знаки пунктуации (например “Аргентина манит негра!”).
        String word = "Аргентина. манит? негра!";
        String cleanWord = word.replaceAll(" ", "").replaceAll("!", "").replaceAll("\\?","").replaceAll("\\.", "");
        StringBuilder s = new StringBuilder(cleanWord);
        s.reverse();
        String palindrom = s.toString();
        System.out.println(palindrom);

        if (cleanWord.equalsIgnoreCase(palindrom)) {
            System.out.println("слово является палиндромом");
        } else {
            System.out.println("слово не является палиндромом");
        }
    }
}